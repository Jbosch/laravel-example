module.exports = {
    platform: 'gitlab',
    endpoint: 'https://gitlab.com/api/v4/',
    assignees: ['jbosch'],
    baseBranches: ['main'],
    labels: ['renovate'],
    repositories: ['Jbosch/laravel-example'],
};